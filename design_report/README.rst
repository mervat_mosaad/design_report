================
Create PDF Report
================
This module used to generate pdf report.


Usage
=====
1- Activate Developer Mode.
===========================
    Setting->Activate the developer mode.

2- Go to Design Reports menuitem.
=================================
    Setting->Design Reports.
    create your pdf report by choice report name, model name and your html tags.

3- Go to view of your model, you will find your report.
=======================================================

4- Ability to edit name and design of created report.
=====================================================
    Setting->Design Reports->[Your Report]->Edit
