# -*- coding: utf-8 -*-

from odoo import models, fields, api
import logging

_logger = logging.getLogger(__name__)

class design_report(models.Model):
    _name = 'general.design.report'
    name = fields.Char('Report Name')
    report_view_id = fields.Many2one('ir.ui.view')
    report_action_id = fields.Many2one('ir.actions.report.xml')
    model_id = fields.Many2one('ir.model', string='Model')

    _template = """
                   <h2>Report title</h2>
                   <p>This objects name is <span t-field="o.name"/></p>
                """

    arch_template = fields.Text(default=_template, store=True)

    @api.model
    def create(self, vals):

        _logger.info('ONCREATE name_model')

        temp = """<?xml version="1.0"?>
                          <t t-name="design_report.general_""" + str(vals['name']) + """_report" """ + """ > """ + """
                                <t t-call="report.html_container">
                                    <t t-foreach="docs" t-as="o">
                                        <t t-call="report.external_layout">
                                            <div class="page">
                                                """ + str(vals['arch_template']) + """
                                            </div>
                                        </t>
                                    </t>
                                </t>
                          </t>"""

        name_model = self.env['ir.model'].search([['id', '=', vals['model_id']]], limit=1).model

        xml = self.env['ir.actions.report.xml'].create({
            'name': 'General_' + str(vals['name']) + '_Report',
            'model': name_model,
            'report_type': 'qweb-pdf',
            'report_name': 'design_report.general_' + str(vals['name']) + '_report',
            'attachment_use': True,
        })

        action = self.env['ir.values'].create({
            'name': xml.name,
            'model': xml.model,
            'key2': 'client_print_multi',
            'action_id': self.env['ir.actions.actions'].search([['name', '=', xml.name]]).id,
            'value_unpickle': 'ir.actions.report.xml,' + str(xml.id),
        })


        identefir = self.env['ir.model.data'].create({
            'module': 'design_report',
            'name': xml.name,
            'model': 'ir.actions.report.xml',
            'res_id': xml.id,
        })

        vals['report_action_id'] = xml.id



        x = self.env['ir.ui.view'].create({
            'name': 'general_' + str(vals['name']) + '_report',
            'mode': 'primary',
            'type': 'qweb',
            'priority': 16,
            'active': True,
            'arch': temp,
        })

        vals['report_view_id'] = x.id

        identefirview = self.env['ir.model.data'].create({
            'module': 'design_report',
            'name': x.name,
            'model': 'ir.ui.view',
            'res_id': x.id,
        })


        Object = super(design_report, self).create(vals)

        return Object

    @api.multi
    def print_report(self):
        _logger.info('Report Test')
        _logger.info(self.env['report'].get_action(self, 'design_report.general_' + str(self.name) + '_report'))
        return self.env['report'].get_action(self, 'design_report.general_' + str(self.name) + '_report')

    @api.multi
    def write(self, vals):
        report_view = self.env['ir.ui.view'].search([['id', '=', self.report_view_id.id]], limit=1)
        identefir = self.env['ir.model.data'].search([['res_id', '=', report_view.id]], limit=1)
        if 'arch_template' in vals:
            temp = """<?xml version="1.0"?>
                                      <t t-name="design_report.general_""" + str(
                self.name) + """_report" """ + """ > """ + """
                                            <t t-call="report.html_container">
                                                <t t-foreach="docs" t-as="o">
                                                    <t t-call="report.external_layout">
                                                        <div class="page">
                                                            """ + str(vals['arch_template']) + """
                                                        </div>
                                                    </t>
                                                </t>
                                            </t>
                                      </t>"""

            if report_view:
                report_view.write({
                    'arch': temp,
                })
        if ('name' in vals) or ('model_id' in vals):
            if 'arch_template' in vals:
                value = str(vals['arch_template'])
            elif 'arch_template' not in vals:
                value = str(self.arch_template)

            action_report = self.env['ir.actions.report.xml'].search([['id', '=', self.report_action_id.id]], limit=1)
            _logger.info('actions Report')
            _logger.info(action_report)
            if action_report:
                action_action = self.env['ir.actions.actions'].search([['name', '=', action_report.name]])
                _logger.info('action_action')
                _logger.info(action_action)
                if action_action:
                    action = self.env['ir.values'].search([['name', '=', action_report.name]], limit=1)
                    _logger.info('action')
                    _logger.info(action)
                    if action:
                        identefir_action_report = self.env['ir.model.data'].search([['res_id', '=', action_report.id]], limit=1)
                        _logger.info('identefir_action_report')
                        _logger.info(identefir_action_report)
                        if identefir_action_report:
                            if 'model_id' in vals:
                                name_model = self.env['ir.model'].search([['id', '=', vals['model_id']]], limit=1).model
                                action_report.write({
                                'model': name_model
                                })

                                action.write({
                                    'model': action_report.model,
                                })

                            if 'name' in vals:

                                temp = """<?xml version="1.0"?>
                                  <t t-name="design_report.general_""" + str(vals['name']) + """_report" """ + """ > """ + """
                                        <t t-call="report.html_container">
                                            <t t-foreach="docs" t-as="o">
                                                <t t-call="report.external_layout">
                                                    <div class="page">
                                                        """ + value + """
                                                    </div>
                                                </t>
                                            </t>
                                        </t>
                                  </t>"""
                                action_report.write({
                                    'name': 'General_' + str(vals['name']) + '_Report',
                                    'report_name': 'design_report.general_' + str(vals['name']) + '_report',
                                })

                                action_action.write({
                                    'name': action_report.name,
                                })

                                action.write({
                                    'name': action_report.name,
                                })

                                identefir_action_report.write({
                                    'name': action_report.name,
                                    'module': 'design_report',
                                })
                                if report_view:
                                    if identefir:
                                        report_view.write({
                                        'name': 'general_' + str(vals['name']) + '_report',
                                        'arch': temp,
                                        })

                                        identefir.write({
                                            'name': report_view.name,
                                            'module': 'design_report',
                                        })
           
        return super(design_report, self).write(vals)

    @api.multi
    def unlink(self):
        action_report = self.env['ir.actions.report.xml'].search([['id', '=', self.report_action_id.id]], limit=1)
        if action_report:
            action_action = self.env['ir.actions.actions'].search([['name', '=', action_report.name]], limit=1)
            if action_action:
                action = self.env['ir.values'].search([['name', '=', action_report.name]], limit=1)
                if action:
                    identefir_action_report = self.env['ir.model.data'].search([['res_id', '=', action_report.id]], limit=1)
                    if identefir_action_report:
                        report_view = self.env['ir.ui.view'].search([['id', '=', self.report_view_id.id]], limit=1)
                        if report_view:
                            identefir_view = self.env['ir.model.data'].search([['res_id', '=', report_view.id]], limit=1)
                            if identefir_view:
                                if action_report.unlink():
                                    _logger.info('DELETED!!')
                                action_action.unlink()
                                action.unlink()
                                identefir_action_report.unlink()
                                report_view.unlink()
                                identefir_view.unlink()

        return super(design_report, self).unlink()

