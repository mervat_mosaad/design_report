# -*- coding: utf-8 -*-

{
    'name': 'Create PDF Report',
    'summary': 'Create and design pdf report',
    'author': 'Mervat Mosaad',
    'category': 'Reporting',
    'version': '1.0',
    'website': 'mervatmosaad96@gmail.com',
    'depends': ['base'],
    'data': [
        'views/view.xml',
    ],
    'installable': True,
    'application': True,

}
